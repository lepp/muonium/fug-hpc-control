#!/usr/bin/env python3


import PySimpleGUI as sg
import argparse
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import serial
import time

import fug_hpc

sg_theme = "Black"
plt_style = "dark_background"
color_u = "blue"
color_i = "red"
font = ("Courier", 20)
# On gryphon a 1ms timeout leads to a cycle duration of ~50ms, i.e. a buffer size of 12000 corresponds to ~10min
win_timeout_ms = 1
down_sampling = 1
buffer_size = 2 ** 16
supported_interfaces = ("serial", "usb", "lan")
ramp_behaviour_dict = {
    "Off": 0,
    "Up/Down": 1,
    "Up": 2
}
ramp_behaviour_inv = {v: k for k, v in ramp_behaviour_dict.items()}


def float_format(num_or_string):
    return f"{float(num_or_string):.3e}"


def software_trip(i_setpoint, i_actual, u, fug):
    if abs(i_actual) > i_setpoint:
        fug.kill()
        sg.popup_error(
            f"Software trip at {time.asctime(time.localtime(time.time()))} with {float_format(u)}V and {float_format(i_actual)}A",
            title=fug_hpc.model)


def fill_buffer(buffer, u, i):
    if not buffer["counter"]:
        buffer["t"][buffer["idx"]] = time.time()
        buffer["u"][buffer["idx"]] = u
        buffer["i"][buffer["idx"]] = i
        buffer["idx"] = (buffer["idx"] + 1) % buffer_size
    buffer["counter"] = (buffer["counter"] + 1) % down_sampling


def roll_buffer(buffer):
    t = np.roll(buffer["t"], -buffer["idx"]) - np.nanmax(buffer["t"])
    u = np.roll(buffer["u"], -buffer["idx"]) / 1e3
    i = np.roll(buffer["i"], -buffer["idx"]) * 1e6
    return t, u, i


def save_buffer(buffer):
    path = sg.popup_get_file("Save history as...", save_as=True, file_types=(("CSV", "*.csv"), ("ALL Files", "*.* *")),
                             default_extension=".csv", keep_on_top=True)
    if path is not None:
        try:
            np.savetxt(path, np.stack(roll_buffer(buffer), axis=-1), delimiter=",")
        except Exception as exc:
            sg.popup_error(f"Failed to save history as {path}: {exc}")


def update_plot(buffer, fig, ax_u, ax_i, title):
    t, u, i = roll_buffer(buffer)
    ax_u.clear()
    ax_u.plot(t, u, color_u)
    ax_u.set_title(title)
    ax_u.set_xlabel("t [s]")
    ax_u.set_ylabel("U [kV]", color=color_u)
    ax_u.tick_params(axis="y", labelcolor=color_u)
    ax_u.grid(which="both", linestyle="--")
    ax_i.clear()
    ax_i.plot(t, i, color_i)
    ax_i.set_ylabel("I [uA]", color=color_i)
    ax_i.yaxis.set_label_position("right")
    ax_i.tick_params(axis="y", labelcolor=color_i)
    fig.canvas.draw_idle()
    fig.canvas.flush_events()


def update_actual_values(win, fug, buffer):
    u = fug.get_u(silent=True)
    i = fug.get_i(silent=True)
    output_state = fug.get_output_state(silent=True)
    overcurrent_state = fug.get_overcurrent_state(silent=True)
    overcurrent_trip_state = fug.get_overcurrent_trip_state(silent=True)
    win["-TUA-"].update(float_format(u))
    win["-TIA-"].update(float_format(i))
    if output_state:
        win["-TO-"].update("ON", text_color="red")
    else:
        win["-TO-"].update("OFF", text_color="green")
    if overcurrent_state:
        win["-TT-"].update("On")
    else:
        win["-TT-"].update("Off")
    if overcurrent_trip_state:
        win["-TTS-"].update("TRIP", text_color="red")
    else:
        win["-TTS-"].update("OK", text_color="green")
    fill_buffer(buffer, u, i)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("interface", help=str(supported_interfaces))
    parser.add_argument("port")
    parser.add_argument("-v", "--verbose", action="count", default=0)
    parser.add_argument("-r", "--no-reset", action="store_true", help=f"Don't reset {fug_hpc.model}")
    parser.add_argument("-l", "--label")
    args = parser.parse_args()
    interface = args.interface
    port = args.port
    verbose = args.verbose
    reset = not args.no_reset
    label = args.label
    win_title = fug_hpc.model
    if label:
        win_title += f" {label}"

    if interface in supported_interfaces[:2]:
        fug = fug_hpc.FugHpcSerial(port, verbose=verbose)
    elif interface == supported_interfaces[2]:
        fug = fug_hpc.FugHpcLAN(port, verbose=verbose)
    else:
        raise RuntimeError(f"Interface {interface} not supported. Supported interfaces are: {supported_interfaces}")
    if reset:
        fug.reset()

    buffer = {
        "idx": 0,
        "counter": 0,
        "t": np.full(buffer_size, np.NaN, dtype=np.longdouble),
        "u": np.full(buffer_size, np.NaN, dtype=np.double),
        "i": np.full(buffer_size, np.NaN, dtype=np.double)
    }

    # Plot will not show up without this
    matplotlib.use("TkAgg")
    plt.style.use(plt_style)
    fig = plt.figure()
    fig.tight_layout()
    ax_u = fig.add_subplot(111)
    ax_i = ax_u.twinx()
    plt.show(block=False)

    sg.theme(sg_theme)
    sg.set_options(font=font)
    layout = [
        [
            sg.Push(),
            sg.T(win_title),
            sg.Push()
        ],
        [
            sg.T("Voltage [V]", size=(12, 1)),
            sg.T("-", key="-TUA-", size=(11, 1)),
            sg.T("Setpoint:", size=(10, 1)),
            sg.T(float_format(fug.get_u_set()), key="-TUS-", size=(11, 1)),
            sg.B("Set", key="-BUS-", size=(4, 1)),
            sg.I(key="-IUS-", size=(11, 1))
        ],
        [
            sg.T("Current [A]", size=(12, 1)),
            sg.T("-", key="-TIA-", size=(11, 1)),
            sg.T("Setpoint:", size=(10, 1)),
            sg.T(float_format(fug.get_i_set()), key="-TIS-", size=(11, 1)),
            sg.B("Set", key="-BIS-", size=(4, 1)),
            sg.I(key="-IIS-", size=(11, 1))
        ],
        [
            sg.T("Overcurrent", size=(12, 1)),
            sg.T("-", key="-TT-", size=(11, 1)),
            sg.B("On", key="-BTO-", size=(8, 1)),
            sg.B("Off", key="-BTK-", size=(10, 1)),
            sg.T("Status", size=(8, 1)),
            sg.T("-", key="-TTS-", size=(7, 1))
        ],
        [
            sg.T("Ramp [V/s]", size=(12, 1)),
            sg.DD(tuple(ramp_behaviour_dict), key="-DRB-", enable_events=True,
                  default_value=ramp_behaviour_inv[fug.get_ramp_behaviour()], size=(10, 1)),
            sg.T("Setpoint:", size=(10, 1)),
            sg.T(float_format(fug.get_ramp_speed()), key="-TRS-", size=(11, 1)),
            sg.B("Set", key="-BRS-", size=(4, 1)),
            sg.I(key="-IRS-", size=(11, 1))
        ],
        [
            sg.T("Output", size=(12, 1)),
            sg.T("-", key="-TO-", size=(11, 1)),
            sg.B("On", key="-BOO-", size=(8, 1)),
            sg.B("KILL", key="-BOK-", button_color="red", size=(10, 1)),
            sg.T("Polarity", size=(8, 1)),
            sg.DD(tuple(fug_hpc.pol_dict.keys()), key="-DP-", enable_events=True, default_value=fug.get_polarity_set(),
                  size=(2, 1))
        ],
        [sg.B("Exit", key="-BE-"), sg.B("Save History", key="-BS-")]
    ]

    win = sg.Window(win_title, layout)

    last_event = None
    while True:
        try:
            event, values = win.read(win_timeout_ms)
            update_actual_values(win, fug, buffer)
            update_plot(buffer, fig, ax_u, ax_i, win_title)
            if (verbose and (event != "__TIMEOUT__")) or (verbose > 2):
                print(f"Processing event {event}...")
            if event != last_event:
                last_event = event
                if event == "-BOK-":
                    fug.kill()
                    fug.reset_overcurrent_trip()
                elif event in ("-BE-", sg.WIN_CLOSED):
                    break
                elif event == "-BUS-":
                    win["-TUS-"].update(float_format(fug.set_u(abs(float(values['-IUS-'])))))
                elif event == "-BIS-":
                    win["-TIS-"].update(float_format(fug.set_i(abs(float(values['-IIS-'])))))
                elif event == "-BTO-":
                    fug.set_overcurrent_state(1)
                elif event == "-BTK-":
                    fug.set_overcurrent_state(0)
                elif event == "-BRS-":
                    win["-TRS-"].update(float_format(fug.set_ramp_speed(abs(float(values['-IRS-'])))))
                elif event == "-DRB-":
                    fug.set_ramp_behaviour(ramp_behaviour_dict[values["-DRB-"]])
                elif event == "-DP-":
                    fug.set_polarity(values["-DP-"])
                elif event == "-BOO-":
                    fug.set_output_state(1)
                elif event == "-BS-":
                    save_buffer(buffer)
        except (fug_hpc.Error, serial.SerialException) as exc:
            sg.popup_error(f"{fug_hpc.model} exception: {exc}")
        except ValueError as exc:
            sg.popup_error(f"Invalid input: {exc}")
        except KeyboardInterrupt:
            break
    win.close()


if __name__ == "__main__":
    main()
