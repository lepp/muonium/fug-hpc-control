# FUG HPC Control

GUI and class to control HPC series power supplies by FUG Elektronik GmbH.

## Requirements

FUG HPC Control has been tested on GNU/Linux.
It might also work on other operating systems where pySerial is available.
The following packages are required:

- Python 3
- pySerial
- PySimpleGUI

A clean and easy way to install these is to install Miniconda (or Anaconda)
from https://docs.conda.io/en/latest/miniconda.html and then create a new environment with the following command:

```shell
$ conda create -n fug python pyserial pysimplegui matplotlib numpy
```

Replace `fug` with a name of your liking.

## Installation

1. Clone this repository
2. If you created a conda environment as described above, you can create a script to automatically source it upon
   start (replace `fug` if you used a different name).

```shell
$ conda activate fug
$ /path/to/fug-hpc-control/create_start_script.sh
```

This will create a script called `start_fug.sh` in the root of the git repo, which can be moved anywhere to directly
start FUG HPC Control.
Note that the start script can only be created for a dedicated conda environment.
It won't work for the base environment, nor for any other means of a virtual environment.
Furthermore, as the script is location independent, it contains absolute paths, and needs to be recreated whenever the
repo is moved.

## Usage

If you created a start script, you can run FUG HPC Control using

```shell
$ ./start_fug.sh [INTERFACE] [PORT]
```

(If you didn't create the start script, you can instead run `/path/to/fug-hpc-control/fug_hpc_gui.py` after making sure
all the required packages are available.)

`[INTERFACE]` is the interface type used to talk to the FUG HPC.
Currently, LAN, serial, and USB interfaces are implemented.
Replace `[INTERFACE]` with either `lan`, `usb`, or `serial`.

For `lan` `[PORT]` should be `[HOST]:[TCP_PORT]`, where host is the hostname or IP address, and `[TCP_PORT]` is the TCP
port the FUG HPC is listening on (2101 by default).
E.g.`./start_fug.sh lan 192.168.2.1:2101`.
For `usb` and `serial` `[PORT]` is the USB or serial port the FUG HPC is connected to.
An easy way to list (USB) serial ports under Linux is looking under `/dev/serial/by-id`.
(The USB option of the FUG HPC is just a USB-to-serial converter, hence the two options are equivalent from a software
point of view). For example

```shell
$ ls /dev/serial/by-id
usb-FTDI_USB__-__Serial-if00-port0
$ ./start_fug.sh usb /dev/serial/by-id/usb-FTDI_USB__-__Serial-if00-port0
```

If you have multiple devices in that folder, you can list its contents before and after connecting the FUG HPC to your
machine, and then select the device that newly appeared.

You can display a quick help using `-h`.

`-v` increases verbosity (use multiple to increase even further).

`-r` prevents resetting the FUG HPC at the start of the application.

`-l LABEL` adds a label to the window. Useful when controlling multiple FUG HPCs simultaneously.

The GUI is divided into five rows of three columns each.
The rows correspond to voltage, current, overcurrent trip, ramp, and output.
The first column displays the current value for voltage, current, overcurrent trip, and output.
For the ramp row this sets the ramp type.
This can be off, up/down, or up, corresponding to no ramping, ramping for raising and lowering
voltage (modulus) or only raising voltage.
The second column contains the current setpoints for all except the overcurrent trip and output row, where it contains
the on and off/kill switches.
Finally, the third column allows changing the setpoints by entering a value into the field right of the set button, and
then pressing the latter.
While for the overcurrent trip it shows the current state. In the third column of the output row the polarity can be
changed.

## Notes and caveats

- An overcurrent trip can be reset by pressing the `KILL` button.
- All set values are moduli.
  The polarity is only chosen by the polarity switch.
  The trip current will operate on the modulus of the current.
  Similarly, up/down for the voltage ramp corresponds to the modulus of the voltage.
- The ramp is never applied when the output is killed / switched off.
  If enabled, it is, however, applied when the output is switched on.
- Error codes: E4 - Invalid argument; E5 - Argument out of range.
  In particular, the FUG HPC will reply with an E5 if the requested voltage/current value is too high.
- Sometimes, there's a communication error on start-up.
  In this case, just try again.
